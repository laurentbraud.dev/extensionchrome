﻿// Fonction pour modifier ou masquer la div sidebar
function modifyDisplay() {
  var sidebar = document.getElementById('sidebar');
  var content = document.getElementById('content');

  if (sidebar.style.display === 'none') {
    sidebar.style.display = 'block';
	
	
	chrome.storage.local.get('redmineSidebarContentWidth', function(result) {
		content.style.width = result.redmineSidebarContentWidth;
	});
	chrome.storage.local.get('redmineSidebarSidebarWidth', function(result) {
		sidebar.style.width = result.redmineSidebarSidebarWidth;
	});

	sidebar.style.width = '22%';
	chrome.storage.local.set({ redmineSidebarHidden: false });
  } else {
    sidebar.style.display = 'none';
	chrome.storage.local.set({ redmineSidebarContentWidth: content.style.width });
	chrome.storage.local.set({ redmineSidebarSidebarWidth: sidebar.style.width });
	
	content.style.width = '97%';
	sidebar.style.width = '0%';
	chrome.storage.local.set({ redmineSidebarHidden: true });
  }
}


chrome.storage.local.get('redmineSidebarHidden', function(result) {
  // Vérifier si la propriété "redmineSidebarHidden" est définie dans le stockage local
  if (result.redmineSidebarHidden) {
    modifyDisplay();
  }
});

// Ajouter un écouteur d'événements pour la touche F2
document.addEventListener('keydown', function(event) {
  if (event.code === 'F2') {
    // Appeler la fonction pour modifier l'attribut "width" de la div avec l'ID "sidebar"
    modifyDisplay();
  }
});